import React from 'react';
import styles from './Home.css';

const Home = () => (
  <div>
    <h2 className={styles.title}>Home</h2>
  </div>
);

export default Home;
